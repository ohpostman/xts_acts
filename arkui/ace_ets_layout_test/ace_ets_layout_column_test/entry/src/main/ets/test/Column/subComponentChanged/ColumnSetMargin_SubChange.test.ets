/*
 * Copyright (c) 2023 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@ohos/hypium";
import router from '@system.router';
import CommonFunc from "../../../MainAbility/common/Common";
import { MessageManager,Callback } from '../../../MainAbility/common/MessageManager';
export default function ColumnSetMargin_SubChange() {
  describe('ColumnSetMarginTest', function () {
    beforeEach(async function (done) {
      let options = {
        uri: 'MainAbility/pages/Column/subcomponentChanged/ColumnSetMargin_SubChange',
      }
      try {
        router.clear();
        await CommonFunc.sleep(1000);
        let pages = router.getState();
        console.info("get SetMargin state success " + JSON.stringify(pages));
        if (!("FlexBase" == pages.name)) {
          console.info("get SetMargin state success " + JSON.stringify(pages.name));
          let result = await router.push(options)
          console.info("push SetMargin page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push SetMargin page error " + JSON.stringify(err));
      }
      await CommonFunc.sleep(2000);
      done()
    });
    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("SetMargin after each called");
    });

    /**
     * @tc.number    SUB_ACE_COLUMN_SUBCOMPONENTCHANGED_0800
     * @tc.name      testColumnParentMarginPadSubMarPad
     * @tc.desc      Parent component set margin,subcomponent set margin and paddding.
     */
    it('testColumnParentMarginPadSubMarPad', 0, async function (done) {
      console.info('new testColumnParentMarginPadSubMarPad START');
      await CommonFunc.sleep(2000);
      let columnSetMargin_1 = CommonFunc.getComponentRect('columnSetMargin_1');
      let columnSetMargin_2 = CommonFunc.getComponentRect('columnSetMargin_2');
      let columnSetMargin_3 = CommonFunc.getComponentRect('columnSetMargin_3');
      let columnSetMargin = CommonFunc.getComponentRect('columnSetMargin');
      expect(Math.round(columnSetMargin_1.top - columnSetMargin.top)).assertEqual(Math.round(vp2px(20)))
      expect(Math.round(columnSetMargin_2.top - columnSetMargin_1.bottom)).assertEqual(Math.round(vp2px(50)))
      expect(Math.round(columnSetMargin_3.top - columnSetMargin_2.bottom)).assertEqual(Math.round(vp2px(30)))
      expect(columnSetMargin.bottom).assertEqual(columnSetMargin_3.bottom)
      expect(Math.round(columnSetMargin_1.right - columnSetMargin_1.left)).assertEqual(Math.round(vp2px(300)))
      expect(Math.round(columnSetMargin_2.right - columnSetMargin_2.left)).assertEqual(Math.round(vp2px(300)))
      expect(Math.round(columnSetMargin_3.right - columnSetMargin_3.left)).assertEqual(Math.round(vp2px(300)))
      console.info('new testColumnParentMarginPadSubMarPad END');
      done();
    });
  })
}
