/*
 * Copyright (c) 2023 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@ohos/hypium";
import router from '@system.router';
import CommonFunc from "./../../../../MainAbility/common/Common"
export default function AlignEnd_NoSpace() {
  describe('AlignEnd_NoSpace', function () {
    beforeEach(async function (done) {
      let options = {
        uri: 'MainAbility/pages/Flex/justifyContent/End/AlignEnd_NoSpace'
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get AlignEnd_NoSpace state success " + JSON.stringify(pages));
        if (!("AlignEnd_NoSpace" == pages.name)) {
          console.info("get AlignEnd_NoSpace state pages.name " + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push AlignEnd_NoSpace page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push AlignEnd_NoSpace page error " + JSON.stringify(err));
        expect().assertFail();
      }
      done()
    });

    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("AlignEnd_NoSpace after each called");
    });

    /**
     * @tc.number    SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_END_TEST_0100
     * @tc.name      testFlexAlignEndFlexWidthOverflow
     * @tc.desc      The parent component layout space does not meet the interface display of the spindle layout of
     * the child component
     */
    it('testFlexAlignEndFlexWidthOverflow', 0, async function (done) {
      console.info('[testFlexAlignEndFlexWidthOverflow] START');
      globalThis.value.message.notify({name:'DadWidth', value:400});
      await CommonFunc.sleep(3000);
      let strJson = getInspectorByKey('End_NoSpace_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Flex');
      expect(obj.$attrs.constructor.direction).assertEqual('FlexDirection.Row');
      expect(obj.$attrs.constructor.justifyContent).assertEqual('FlexAlign.End');
      let End_NoSpace_011 = CommonFunc.getComponentRect('End_NoSpace_011');
      let End_NoSpace_012 = CommonFunc.getComponentRect('End_NoSpace_012');
      let End_NoSpace_013 = CommonFunc.getComponentRect('End_NoSpace_013');
      let End_NoSpace_01 = CommonFunc.getComponentRect('End_NoSpace_01');
      expect(End_NoSpace_011.top).assertEqual(End_NoSpace_012.top);
      expect(End_NoSpace_012.top).assertEqual(End_NoSpace_013.top);
      expect(End_NoSpace_011.top).assertEqual(End_NoSpace_01.top);
      expect(End_NoSpace_011.left).assertEqual(End_NoSpace_01.left);
      expect(End_NoSpace_012.left).assertEqual(End_NoSpace_011.right);
      expect(End_NoSpace_013.left).assertEqual(End_NoSpace_012.right);
      expect(End_NoSpace_013.right).assertEqual(End_NoSpace_01.right);
      expect(Math.round(End_NoSpace_011.bottom - End_NoSpace_011.top)).assertEqual(Math.round(vp2px(50)));
      expect(Math.round(End_NoSpace_012.bottom - End_NoSpace_012.top)).assertEqual(Math.round(vp2px(100)));
      expect(Math.round(End_NoSpace_013.bottom - End_NoSpace_013.top)).assertEqual(Math.round(vp2px(150)));
      expect(Math.round(End_NoSpace_011.right - End_NoSpace_011.left)).assertEqual(Math.round(vp2px(400/3)));
      expect(Math.round(End_NoSpace_012.right - End_NoSpace_012.left)).assertEqual(Math.round(vp2px(400/3)));
      expect(Math.round(End_NoSpace_013.right - End_NoSpace_013.left)).assertEqual(Math.round(vp2px(400/3)));
      console.info('[testFlexAlignEndFlexWidthOverflow] END');
      done();
    });
    /**
     * @tc.number    SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_END_TEST_0200
     * @tc.name      testFlexAlignEndFlexWidthMeet
     * @tc.desc      The interface display where the parent component layout space satisfies the spindle layout of
     * the child component
     */
    it('testFlexAlignEndFlexWidthMeet', 0, async function (done) {
      console.info('[testFlexAlignEndFlexWidthMeet] START');
      globalThis.value.message.notify({name:'DadWidth', value:500})
      await CommonFunc.sleep(3000);
      let strJson = getInspectorByKey('End_NoSpace_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Flex');
      expect(obj.$attrs.constructor.direction).assertEqual('FlexDirection.Row');
      expect(obj.$attrs.constructor.justifyContent).assertEqual('FlexAlign.End');
      let End_NoSpace_011 = CommonFunc.getComponentRect('End_NoSpace_011');
      let End_NoSpace_012 = CommonFunc.getComponentRect('End_NoSpace_012');
      let End_NoSpace_013 = CommonFunc.getComponentRect('End_NoSpace_013');
      let End_NoSpace_01 = CommonFunc.getComponentRect('End_NoSpace_01');
      expect(End_NoSpace_011.top).assertEqual(End_NoSpace_012.top);
      expect(End_NoSpace_012.top).assertEqual(End_NoSpace_013.top);
      expect(End_NoSpace_011.top).assertEqual(End_NoSpace_01.top);
      expect(End_NoSpace_012.left).assertEqual(End_NoSpace_011.right);
      expect(End_NoSpace_013.left).assertEqual(End_NoSpace_012.right);
      expect(End_NoSpace_013.right).assertEqual(End_NoSpace_01.right);
      expect(Math.round(End_NoSpace_011.left - End_NoSpace_01.left)).assertEqual(Math.round(vp2px(50)));
      expect(Math.round(End_NoSpace_011.bottom - End_NoSpace_011.top)).assertEqual(Math.round(vp2px(50)));
      expect(Math.round(End_NoSpace_012.bottom - End_NoSpace_012.top)).assertEqual(Math.round(vp2px(100)));
      expect(Math.round(End_NoSpace_013.bottom - End_NoSpace_013.top)).assertEqual(Math.round(vp2px(150)));
      expect(Math.round(End_NoSpace_011.right - End_NoSpace_011.left)).assertEqual(Math.round(vp2px(150)));
      expect(Math.round(End_NoSpace_012.right - End_NoSpace_012.left)).assertEqual(Math.round(vp2px(150)));
      expect(Math.round(End_NoSpace_013.right - End_NoSpace_013.left)).assertEqual(Math.round(vp2px(150)));
      console.info('[testFlexAlignEndFlexWidthMeet] END');
      done();
    });
  })
}
