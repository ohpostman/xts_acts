/**
 * Copyright (c) 2023 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium';
import router from '@ohos.router';
import CommonFunc from '../../../MainAbility/common/Common';
import { UiComponent, UiDriver, Component, Driver, UiWindow, ON, BY, MatchPattern, DisplayRotation, ResizeDirection,
  WindowMode, PointerMatrix, UiDirection, MouseButton } from '@ohos.UiTest';
export default function Tabs_itemSize() {
  describe('Tabs_itemSize', function () {
    beforeEach(async function (done) {
      console.info("Tabs_itemSize beforeEach start");
      let options = {
        url: "MainAbility/pages/Tabs/Tabs_ItemsChange/Tabs_itemSize",
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get Tabs_itemSize state pages:" + JSON.stringify(pages));
        if (!("Tabs_itemSize" == pages.name)) {
          console.info("get Tabs_itemSize pages.name:" + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push Tabs_itemSize page result:" + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push Tabs_itemSize page error " + JSON.stringify(err));
        expect().assertFail();
      }
      console.info("Tabs_itemSize beforeEach end");
      done();
    });
    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("Tabs_itemSize after each called")
      globalThis.value.message.notify({name:'currentIndex', value:0});
    });
    /**
     * @tc.number    SUB_ACE_TABS_ITEMSIZE_TEST_0100
     * @tc.name      testTabsItemSize
     * @tc.desc      Set width to 200 and height to 200 for TabContent
     */
    it('testTabsItemSize', 0, async function (done) {
      console.info('[testTabsItemSize] START');
      await CommonFunc.sleep(3000);
      let strJson = getInspectorByKey('Tabs_itemSize_01');
      let obj = JSON.parse(strJson);
      console.info(`[testTabsItemSize] type: ${JSON.stringify(obj.$type)}`);
      console.info("[testTabsItemSize] barPosition: " + JSON.stringify(obj.$attrs.barPosition));
      console.info("[testTabsItemSize] index: " + JSON.stringify(obj.$attrs.index));
      console.info("[testTabsItemSize] scrollable: " + JSON.stringify(obj.$attrs.scrollable));
      console.info("[testTabsItemSize] barMode: " + JSON.stringify(obj.$attrs.barMode));
      expect(obj.$type).assertEqual('Tabs');
      expect(obj.$attrs.barPosition).assertEqual("BarPosition.Start");
      expect(obj.$attrs.index).assertEqual("0");
      expect(obj.$attrs.scrollable).assertEqual(true);
      expect(obj.$attrs.barMode).assertEqual('BarMode.Fixed');
      let Tabs_itemSize_001 = CommonFunc.getComponentRect('Tabs_itemSize_001');
      let Tabs_itemSize_011 = CommonFunc.getComponentRect('Tabs_itemSize_011');
      let driver = await Driver.create();
      await driver.swipe(Math.round(Tabs_itemSize_011.right - 30),
      Math.round(Tabs_itemSize_011.top + ((Tabs_itemSize_011.bottom - Tabs_itemSize_011.top) / 2)),
      Math.round(Tabs_itemSize_011.left + 30),
      Math.round(Tabs_itemSize_011.top + ((Tabs_itemSize_011.bottom - Tabs_itemSize_011.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_itemSize_002 = CommonFunc.getComponentRect('Tabs_itemSize_002');
      let Tabs_itemSize_012 = CommonFunc.getComponentRect('Tabs_itemSize_012');
      await driver.swipe(Math.round(Tabs_itemSize_012.right - 30),
      Math.round(Tabs_itemSize_012.top +((Tabs_itemSize_012.bottom - Tabs_itemSize_012.top) / 2)),
      Math.round(Tabs_itemSize_012.left + 30),
      Math.round(Tabs_itemSize_012.top + ((Tabs_itemSize_012.bottom - Tabs_itemSize_012.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_itemSize_003 = CommonFunc.getComponentRect('Tabs_itemSize_003');
      let Tabs_itemSize_013 = CommonFunc.getComponentRect('Tabs_itemSize_013');
      let Tabs_itemSize_01 = CommonFunc.getComponentRect('Tabs_itemSize_01');
      let subGreen = CommonFunc.getComponentRect('Tabs_itemSize_green');
      let subBlue = CommonFunc.getComponentRect('Tabs_itemSize_blue');
      let subYellow = CommonFunc.getComponentRect('Tabs_itemSize_yellow');

      console.info(`[testTabsItemSize]Tabs_itemSize_011.left equal Tabs_itemSize_001.left=
        ${Tabs_itemSize_011.left} === ${Tabs_itemSize_001.left}`);
      expect(Tabs_itemSize_011.left).assertEqual(Tabs_itemSize_001.left);
      expect(Tabs_itemSize_012.left).assertEqual(Tabs_itemSize_002.left);
      expect(Tabs_itemSize_013.left).assertEqual(Tabs_itemSize_003.left);
      expect(Tabs_itemSize_011.top).assertEqual(Tabs_itemSize_001.top);
      expect(Tabs_itemSize_012.top).assertEqual(Tabs_itemSize_002.top);
      expect(Tabs_itemSize_013.top).assertEqual(Tabs_itemSize_003.top);

      console.info(`[testTabsItemSize]Tabs_itemSize_011.left equal Tabs_itemSize_01.left=
        ${Tabs_itemSize_011.left} === ${Tabs_itemSize_01.left}`);
      expect(Tabs_itemSize_011.left).assertEqual(Tabs_itemSize_01.left);
      expect(Tabs_itemSize_012.left).assertEqual(Tabs_itemSize_01.left);
      expect(Tabs_itemSize_013.left).assertEqual(Tabs_itemSize_01.left);
      expect(Tabs_itemSize_011.top).assertEqual(subGreen.bottom);
      expect(Tabs_itemSize_012.top).assertEqual(subBlue.bottom);
      expect(Tabs_itemSize_013.top).assertEqual(subYellow.bottom);

      console.info(`[testTabsItemSize]Tabs_itemSize_011.bottom - Tabs_itemSize_011.top=
        ${Math.round(Tabs_itemSize_011.bottom - Tabs_itemSize_011.top)}`);
      expect(Math.round(Tabs_itemSize_011.bottom - Tabs_itemSize_011.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_itemSize_012.bottom - Tabs_itemSize_012.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_itemSize_013.bottom - Tabs_itemSize_013.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_itemSize_011.right - Tabs_itemSize_011.left)).assertEqual(Math.round(vp2px(330)));
      expect(Math.round(Tabs_itemSize_012.right - Tabs_itemSize_012.left)).assertEqual(Math.round(vp2px(330)));
      expect(Math.round(Tabs_itemSize_013.right - Tabs_itemSize_013.left)).assertEqual(Math.round(vp2px(330)));

      console.info(`[testTabsItemSize]Tabs_itemSize_001.bottom - Tabs_itemSize_001.top=
        ${Math.round(Tabs_itemSize_001.bottom - Tabs_itemSize_001.top)}`);
      expect(Math.round(Tabs_itemSize_001.bottom - Tabs_itemSize_001.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_itemSize_002.bottom - Tabs_itemSize_002.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_itemSize_003.bottom - Tabs_itemSize_003.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_itemSize_001.right - Tabs_itemSize_001.left)).assertEqual(Math.round(vp2px(330)));
      expect(Math.round(Tabs_itemSize_002.right - Tabs_itemSize_002.left)).assertEqual(Math.round(vp2px(330)));
      expect(Math.round(Tabs_itemSize_003.right - Tabs_itemSize_003.left)).assertEqual(Math.round(vp2px(330)));

      console.info(`[testTabsItemSize]subGreen.bottom - subGreen.top = ${Math.round(subGreen.bottom - subGreen.top)}`);
      expect(Math.round(subGreen.bottom - subGreen.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subBlue.bottom - subBlue.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subYellow.bottom - subYellow.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subGreen.right - subGreen.left)).assertEqual(Math.round(vp2px(110)));
      expect(Math.round(subBlue.right - subBlue.left)).assertEqual(Math.round(vp2px(110)));
      expect(Math.round(subYellow.right - subYellow.left)).assertEqual(Math.round(vp2px(110)));

      console.info('[testTabsItemSize] END');
      done();
    });
  })
}
