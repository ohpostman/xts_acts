/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import btAccessTest from './BluetoothAccess.test.js'
import btManagerSwitchTest from './BtManagerSetSwitch.test.js'
import btLocalNameTest from './BtSetLocalName.test.js'
import btSwitchTest from './BtSetSwitch.test.js'
import btSppTest from './BtSpp.test.js'
import btSubscBleTest from './BtSysSubscBle.test.js'
import btManagerSppTest from './BtManagerSpp.test.js'
import btSocketTest from './BtSocket.test.js'
import btConstantTest from './BluetoothConstant.test.js'
import btGattAdvertTest from './BtGattAdvertiser.test.js'

export default function testsuite() {
    btAccessTest()
    btConstantTest()
    btSocketTest()
    btLocalNameTest()
    btSwitchTest()
    btSppTest()
    btSubscBleTest()
    btManagerSppTest()
    btManagerSwitchTest()
    btGattAdvertTest()
}
