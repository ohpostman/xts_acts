/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import secureElement from '@ohos.secureElement';
import {describe, beforeAll, beforeEach, afterEach, afterAll, it, expect} from '@ohos/hypium';

function sleep(delay) { // delay x ms
    let start = (new Date()).getTime();
    while ((new Date()).getTime() - start < delay) {
        continue;
    }
}

let Service = null;
let Reader = null;
let Session = null;
let getReader = null;
let nfcSEService = null;

async function getSEService() {
    return new Promise((resolve, reject) => {
        Service = secureElement.newSEService("serviceState", (state) => {
            if (state == secureElement.ServiceState.DISCONNECTED) {
                console.info("[nfc_test] getSEService newService state is Disconnected");
                let err = null;
                err.code = -1;
                err.message = "newSEService failed, service is not connected"
                reject(err);
            } else {
                console.info("[nfc_test] getSEService newService state is Connected");
                resolve(Service);
            }
        });
    });
}

export default function openSessionTest() {
    describe('openSessionTest', function () {
        beforeAll(async function (done) {
            await getSEService().then(async (data) => {
                Service = data;
                let seIsConnected = Service.isConnected();
                console.info("[NFC_test] SEService isConnected The connection status is: " + seIsConnected);
                if (seIsConnected) {
                    getReader = Service.getReaders();
                    Reader = getReader[0];
                    let readerIsPresent = Reader.isSecureElementPresent();
                    console.info("[NFC_test] Reader isConnected The connection status is: " + readerIsPresent);
                    if (readerIsPresent) {
                        Session = Reader.openSession();
                        let sessionIsClosed = Session.isClosed();
                        console.info("[NFC_test] Session isConnected The connection status is: " + sessionIsClosed);
                    }
                }
            }).catch((err) =>{
                console.info("[NFC_test] getSEService err.code " + err.code + "err.message " + err.message);
            })
            done();
        })
        beforeEach(function() {
            console.info('beforeEach called');
        })
        afterEach(function () {
            console.info('afterEach called');
        })
        afterAll(function () {
            nfcSEService.shutdown();
            sleep(5000);
            console.info('[nfc_test] afterAll Se_session shutdown success');
        })

        /**
         * @tc.number SUB_Communication_NFC_secureElement_js_0600
         * @tc.name Test getReader
         * @tc.desc Obtains the reader that provides the session.
         * @tc.type Function
         * @tc.level Level 2
         */
        it('SUB_Communication_NFC_secureElement_js_0600', 0, function () {
            try {
                if (getReader == undefined) {
                    console.info("[NFC_test]6 This function is not supported because the phone NFC chip is ST chip.");
                } else {
                    let getNfcreader = Session.getReader();
                    console.info("[NFC_test]6 Reader result of this session: " + getNfcreader);
                    expect(getNfcreader instanceof Object).assertTrue();
                }
            } catch (error) {
                console.info("[NFC_test]6 The reader result of the session is abnormal:" + error);
                expect().assertFail();
            }
        })

        /**
         * @tc.number SUB_Communication_NFC_secureElement_js_0700
         * @tc.name Test getATR
         * @tc.desc Obtain the ATR of the SE.
         * @tc.type Function
         * @tc.level Level 2
         */
        it('SUB_Communication_NFC_secureElement_js_0700', 0, function ()  {
            try {
                if (getReader == undefined) {
                    console.info("[NFC_test]7 This function is not supported because the phone NFC chip is ST chip.");
                } else {
                    let nfcGetart = Session.getATR();
                    expect(nfcGetart).assertInstanceOf('Array');
                    expect(!nfcGetart == false ).assertTrue();
                    console.info("[NFC_test]7 Get the ATR of this SE: " + nfcGetart);
                }
            } catch (error) {
                console.info("[NFC_test]7 Get the ATR of this SE occurs exception:" + error);
                expect().assertFail();
            }
        })

        /**
         * @tc.number SUB_Communication_NFC_secureElement_js_0800
         * @tc.name Test close Session
         * @tc.desc Close a single session.
         * @tc.type Function
         * @tc.level Level 2
         */
        it('SUB_Communication_NFC_secureElement_js_0800', 0, function ()  {
            try {
                if (getReader == undefined) {
                    console.info("[NFC_test]8 This function is not supported because the phone NFC chip is ST chip.");
                } else {
                    let isopenSession = Session.isClosed();
                    console.info("[NFC_test]8 Check the first one session is open: " + isopenSession);
                    expect(isopenSession).assertFalse();
                    Session.close();
                    sleep(3000)
                    console.info("[NFC_test]8 second session is closed successfully");
                    let iscloseSession = Session.isClosed();
                    console.info("[NFC_test]8 After close Check the first one session is open: " + iscloseSession);
                    expect(iscloseSession).assertTrue();
                }
            } catch (error) {
                console.info("[NFC_test]8 0800 occurs exception:" + error);
                expect().assertFail();
            }
        })

        console.info("*************[nfc_test] start nfc js unit test end*************");
    })
}