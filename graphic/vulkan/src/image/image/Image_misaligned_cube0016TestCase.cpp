/*
 * Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <climits>
#include <gtest/gtest.h>
#include "../ImageBaseFunc.h"
#include "../ActsImage0017TestSuite.h"
#include "shrinkdefine.h"

using namespace std;
using namespace testing::ext;
using namespace OHOS;

static SHRINK_HWTEST_F(ActsImage0017TS, TC7, "dEQP-VK.image.misaligned_cube.7*");
static SHRINK_HWTEST_F(ActsImage0017TS, TC8, "dEQP-VK.image.misaligned_cube.8*");
static SHRINK_HWTEST_F(ActsImage0017TS, TC9, "dEQP-VK.image.misaligned_cube.9*");
static SHRINK_HWTEST_F(ActsImage0017TS, TC10, "dEQP-VK.image.misaligned_cube.10*");
static SHRINK_HWTEST_F(ActsImage0017TS, TC11, "dEQP-VK.image.misaligned_cube.11*");